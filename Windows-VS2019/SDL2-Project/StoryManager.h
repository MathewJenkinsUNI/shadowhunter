#pragma once

#include <string>
class StoryManager
{
public:
	void startStory();
	void endStory(bool died);
	void randomComment();
};

