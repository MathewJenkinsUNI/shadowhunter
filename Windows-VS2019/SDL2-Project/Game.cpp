#include "Game.h"
#include "TextureUtils.h"

#include "Player.h"
#include "NPC.h"
#include "Bullet.h"
#include "Vector2f.h"
#include "AABB.h"
#include "StoryManager.h"

//for printf
#include <cstdio>

// for exceptions
#include <stdexcept>

Game::Game() 
{
    gameWindow = nullptr;
    gameRenderer = nullptr;

    backgroundTexture = nullptr;
    player = nullptr;

    maxPoints = 2500;
    amountOfEnemyDeaths = 0;
    keyStates = nullptr;
    mousePressed = false;
    
    currentRound = 1;
    amountOfEnemiesToSpawn = 20;
    enemies.resize(amountOfEnemiesToSpawn);

    mousePosX = 0;
    mousePosY = 0;

    mouseVector2 = nullptr;

    quit = false;
}

void Game::init()
{
     gameWindow = SDL_CreateWindow("Assignment Project",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags  
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          throw std::runtime_error("Error - SDL could not create renderer\n");          
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        throw std::runtime_error("Error - SDL could not create Window\n");
    }
    
    // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/background.png", gameRenderer);

    if(backgroundTexture == nullptr)
        throw std::runtime_error("Background image not found\n");

    //setup player
    player = new Player();
    player->init(gameRenderer);
    player->setGame(this);

    story->startStory();

    for (int i = 0; i < enemies.size(); i++)
    {
        enemies[i] = new NPC();
        enemies[i]->init(gameRenderer);
        enemies[i]->setGame(this);
        enemies[i]->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

        if (i == enemies.size() - 1)
        {
            printf("Starting Round: %d\n", currentRound);
        }
    }

    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)
}

Game::~Game()
{
    //Clean up!
    delete player;
    player = nullptr;  


    for (int i = 0; i < enemies.size(); i++)
    {
        delete enemies[i];
    }

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end();)
    {   
        delete *it;
        *it = nullptr;
        it = bullets.erase(it);
    } 
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   
}

void Game::draw()
{
     // 1. Clear the screen
    SDL_RenderClear(gameRenderer);

    // 2. Draw the scene
    SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

    player->draw(gameRenderer);

    for (int i = 0; i < enemies.size(); i++)
        enemies[i]->draw(gameRenderer);

    for(int i = 0; i < bullets.size(); i++)
        bullets[i]->draw(gameRenderer);

    // 2.5 Draw HUD
    /*if(player->getScore() < maxPoints)
        printf("Player Score: %d\n", player->getScore());*/
    /*if(player->getScore() >= maxPoints)
        printf("YOU WIN\n");   */

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    
}

void Game::update(float timeDelta)
{
    player->update(timeDelta);

   // if(npc->isDead())
   //     npc->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);

    for (int i = 0; i < enemies.size(); i++)
    {
        enemies[i]->update(timeDelta);
    }

    for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
    {
        if((*it)->hasExpired())
        {
            delete *it;
            *it = nullptr;
            it = bullets.erase(it);
        }
        else
        {
            ++it;
        }
    }

    for (vector<Bullet*>::iterator it = bullets.begin() ; it != bullets.end(); ++it)
    {
        (*it)->update(timeDelta);
    }

    collisionDetection();

    
    if (amountOfEnemyDeaths >= enemies.size())
    {
        clearEnemies();
    }

    if (player->health <= 0)
    {
        story->endStory(true);
        quit = true;
    }
}

void Game::processInputs()
{
    SDL_Event event;

    // Handle input 
    if( SDL_PollEvent( &event ))  // test for events
    { 
        switch(event.type) 
        { 
            case SDL_QUIT:
                quit = true;
            break;

            // Key pressed event
            case SDL_KEYDOWN:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    quit = true;
                    break;
                case SDLK_LSHIFT:
                    player->changeSpeed(true);
                    break;
                }
            break;

            // Key released event
            case SDL_KEYUP:
                switch (event.key.keysym.sym)
                {
                case SDLK_ESCAPE:
                    //  Nothing to do here.
                    break;
                case SDLK_LSHIFT:
                    player->changeSpeed(false);
                    break;
                }
            break;

            // Key pressed event
            case SDL_MOUSEBUTTONDOWN:
                mousePressed = true;
                break;

                // Key released event
            case SDL_MOUSEBUTTONUP:
                mousePressed = false;
                break;
            
            default:
                // not an error, there's lots we don't handle. 
                break;
        }
    }

    SDL_GetMouseState(&mousePosX, &mousePosY);
    mouseVector2 = new Vector2f((float)mousePosX, (float)mousePosY);
    mouseVector2->sub(player->getPosition());

    // Process Inputs - for player 
    player->processInput(keyStates, mouseVector2);
}

Player* Game::getPlayer() 
{
    return player;
}

void Game::runGameLoop()
{
    // Timing variables
    unsigned int currentTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;
    unsigned int prevTimeIndex;

    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();

    // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist 
        //- https://gafferongames.com/post/fix_your_timestep/
        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Process inputs
        processInputs();

        // Update Game Objects
        update(timeDeltaInSeconds);

        //Draw stuff here.
        draw();

        SDL_Delay(1);
    }
}

void Game::createBullet(Vector2f* position, Vector2f* velocity, bool hitPlayer)
{
    Bullet* bullet = new Bullet();

    bullet->init(gameRenderer, position, velocity, hitPlayer);

    bullets.push_back(bullet);
}

void Game::ShowScore()
{
    if (player->getScore() < maxPoints)
        printf("\n Player Score: %d\n", player->getScore());
    else if (player->getScore() >= maxPoints)
    {
        story->endStory(false);
        quit = true;
    }
}

void Game::collisionDetection()
{
    for (int i = 0; i < enemies.size(); i++)
    {
        for (vector<Bullet*>::iterator it = bullets.begin(); it != bullets.end();)
        {
            if (enemies[i]->getAABB() != nullptr)
            {
                if ((*it)->getAABB()->intersects(enemies[i]->getAABB()) && enemies[i]->state != enemies[i]->DEAD && (*it)->hitPlayer == false)
                {
                    // damage npc
                    enemies[i]->takeDamage((*it)->getDamage());

                    // destroy arrow (or it'll keep hitting!)
                    delete* it;
                    *it = nullptr;
                    it = bullets.erase(it);
                }
                else if ((*it)->getAABB()->intersects(player->getAABB()) && (*it)->hitPlayer == true)
                {
                    player->takeDamage(1);

                    // destroy arrow (or it'll keep hitting!)
                    delete* it;
                    *it = nullptr;
                    it = bullets.erase(it);
                }
                else
                {
                    ++it;
                }
            }
        }

        if (enemies[i]->getAABB()->intersects(player->getAABB()) && enemies[i]->state != enemies[i]->DEAD)
        {
            player->takeDamage(5);
        }
    }
}

void Game::spawnEnemies(int amountToSpawn)
{
    enemies.clear();
    enemies.resize(amountToSpawn);

    for (int i = 0; i < enemies.size(); i++)
    {
        enemies[i] = new NPC();
        enemies[i]->init(gameRenderer);
        enemies[i]->setGame(this);
        enemies[i]->respawn(WINDOW_HEIGHT, WINDOW_WIDTH);
        enemies[i]->draw(gameRenderer);

        if (i == enemies.size() - 1)
        {
            currentRound++;
            story->randomComment();
            printf("\n NEXT ROUND: %d\n", currentRound);
            ShowScore();
        }
    }
}

void Game::clearEnemies()
{
    amountOfEnemyDeaths = 0;

    for (int i = 0; i < enemies.size(); i++)
    {
        delete enemies[i];
        if (i = enemies.size())
        {
            amountOfEnemiesToSpawn++;
            spawnEnemies(amountOfEnemiesToSpawn);
        }
    }
}