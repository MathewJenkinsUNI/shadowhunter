#ifndef GAME_H_
#define GAME_H_

#include "SDL2Common.h"
#include "StoryManager.h"

#include <vector>
using std::vector;

class Player;
class NPC;
class Bullet;
class Vector2f;
class StoryManager;

class Game 
{
private:
    // Declare window and renderer objects
    SDL_Window*	    gameWindow;
    SDL_Renderer*   gameRenderer;

    // Background texture
    SDL_Texture*    backgroundTexture;
    
    //Declare Player
    Player*         player;
    StoryManager*   story;

    // Bullets
    vector<Bullet*> bullets;
    vector<NPC*> enemies;
    // Window control 
    bool            quit;
    
    
    // Keyboard
    const Uint8     *keyStates;

    // End goal
    int maxPoints;
    int amountOfEnemiesToSpawn;
    int currentRound;

    // event
    SDL_Event e;

    // Game loop methods. 
    void processInputs();
    void update(float timeDelta);
    void draw();

public:
    // Constructor 
    Game();
    ~Game();

    // Methods
    void init();
    void runGameLoop();
    Player* getPlayer();
    void createBullet(Vector2f* position, Vector2f* velocity, bool hitPlayer);
    void ShowScore();
    void collisionDetection();
    void spawnEnemies(int amountToSpawn);
    void clearEnemies();

    // Public attributes
    static const int WINDOW_WIDTH = 1280;
    static const int WINDOW_HEIGHT= 1024;

    bool            mousePressed;
    int amountOfEnemyDeaths;

    int mousePosX, mousePosY;
    Vector2f* mouseVector2;
};

#endif